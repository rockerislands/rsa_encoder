﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSA_encoder
{
    class prime
    {
        internal readonly Int64 _limit;
        public static bool[] IsPrimes;
        public prime(Int64 limit)
        {
            _limit = limit;
            FindPrimes();
        }
        void FindPrimes()
        {
            IsPrimes = new bool[_limit + 1];
            double sqrt = Math.Sqrt(_limit);
            var limit = _limit;
            for (long x = 1; x <= sqrt; x++)
                for (long y = 1; y <= sqrt; y++)
                {
                    long x2 = x * x;
                    long y2 = y * y;
                    long n = 4 * x2 + y2;
                    if (n <= limit && (n % 12 == 1 || n % 12 == 5))
                        IsPrimes[n] ^= true;

                    n -= x2;
                    if (n <= limit && n % 12 == 7)
                        IsPrimes[n] ^= true;

                    n -= 2 * y2;
                    if (x > y && n <= limit && n % 12 == 11)
                        IsPrimes[n] ^= true;
                }

            for (long n = 5; n <= sqrt; n += 2)
                if (IsPrimes[n])
                {
                    long s = n * n;
                    for (long k = s; k <= limit; k += s)
                        IsPrimes[k] = false;
                }
            IsPrimes[2] = true;
            IsPrimes[3] = true;
        }
        public void FindPrimes_int()
        {

            IsPrimes = new bool[_limit + 1];
            double sqrt = Math.Sqrt(_limit);
            if (sqrt < 29301)
            {
                for (int x = 1; x <= sqrt; x++)
                    for (int y = 1; y <= sqrt; y++)
                    {
                        int x2 = x * x;
                        int y2 = y * y;
                        int n = 4 * x2 + y2;
                        if ((Int64)n <= _limit && (n % 12 == 1 || n % 12 == 5))
                            IsPrimes[n] ^= true;

                        n -= x2;
                        if ((Int64)n <= _limit && n % 12 == 7)
                            IsPrimes[n] ^= true;

                        n -= 2 * y2;
                        if (x > y && (Int64)n <= _limit && n % 12 == 11)
                            IsPrimes[n] ^= true;
                    }

                for (int n = 5; n <= sqrt; n += 2)
                    if (IsPrimes[n])
                    {
                        int s = n * n;
                        for (int k = s; (Int64)k <= _limit; k += s)
                            IsPrimes[k] = false;
                    }
            }
            else
            {
                var limit = (long)_limit;
                for (long x = 1; x <= sqrt; x++)
                    for (long y = 1; y <= sqrt; y++)
                    {
                        long x2 = x * x;
                        long y2 = y * y;
                        long n = 4 * x2 + y2;
                        if (n <= limit && (n % 12 == 1 || n % 12 == 5))
                            IsPrimes[n] ^= true;

                        n -= x2;
                        if (n <= limit && n % 12 == 7)
                            IsPrimes[n] ^= true;

                        n -= 2 * y2;
                        if (x > y && n <= limit && n % 12 == 11)
                            IsPrimes[n] ^= true;
                    }

                for (long n = 5; n <= sqrt; n += 2)
                    if (IsPrimes[n])
                    {
                        long s = n * n;
                        for (long k = s; k <= limit; k += s)
                            IsPrimes[k] = false;
                    }
            }
            IsPrimes[2] = true;
            IsPrimes[3] = true;
        }
    }
}
